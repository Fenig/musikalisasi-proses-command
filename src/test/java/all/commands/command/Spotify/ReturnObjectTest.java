package all.commands.command.Spotify;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReturnObjectTest {

    @Test
    public void getResult() {
        ReturnObject r = new ReturnObject("a", "b");
        assertEquals("a", r.getResult());
    }

    @Test
    public void getUrl() {
        ReturnObject r = new ReturnObject("a", "b");
        assertEquals("b", r.getUrl());
    }
}