package all.commands.command.Spotify.feature;

import org.junit.Test;

import static org.junit.Assert.*;

public class TopTrackCommandTest {

    @Test
    public void getApiResponse() {
        try {
            String c;
            c = new TopTrackCommand().getApiResponse("iu");
            assertNotNull("getApiResponse should not return null", c);
        } catch (Exception e) {}
    }

    @Test
    public void getResult() {
        try {
            String[] c;
            c = new TopTrackCommand().getResult("iu");
            assertNotNull("getResult should not return null", c);
        } catch (Exception e) {}
    }
}