package all.commands.command.Genius;

import org.junit.Test;

import static org.junit.Assert.*;

public class GeniusCommandTest {

    @Test
    public void getApiResponse() {
        try {
            String c;
            c = new GeniusCommand().getApiResponse("iu");
            assertNotNull("getApiResponse should not return null", c);
        } catch (Exception e) {}
    }

    @Test
    public void getResult() {
        try {
            String[] c;
            c = new GeniusCommand().getResult("iu");
            assertNotNull("getResult should not return null", c);
        } catch (Exception e) {}
    }

    @Test
    public void getLyrics() {
        try {
            String c;
            c = new GeniusCommand().getLyrics("iu");
            assertNotNull("getResult should not return null", c);
        } catch (Exception e) {}
    }

    @Test
    public void URLify() {
        String c,d;
        c = new GeniusCommand().URLify("abab");
        d = new GeniusCommand().URLify("a b");
        assertEquals(c,"abab");
        assertEquals(d,"a%20b");
    }
}