package all.commands.command.Genius;


import all.commands.command.Spotify.Command;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


public class GeniusCommand extends Command implements Genius_URL {

    protected String getApiResponse(String SongTitle) throws IOException, ParseException {
        final String token = "15JH0HDsW61jyZoWpLXvOKd4_X8XHU3M5lQgbcBD6NBVH0wkvbblRhNFv0n8txVq";
        final String title = "q=" + SongTitle;
        final String accesstoken = "access_token=" + token;
        final String search = URLify(search_url + title + "&" + accesstoken);
        String result;

        URL url = new URL(search);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("GET");
        conn.setUseCaches(false);


        conn.connect();
        int responsecode = conn.getResponseCode();

        if (responsecode != 200)
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        else {
            String inputLine;

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            result = response.toString();
            conn.disconnect();
        }

        return result;
    }

    public String[] getResult(String query) {
        String result[] = new String[2];
        try {

            JsonParser parser = new JsonParser();
            JsonObject resp = parser.parse(getApiResponse(query))
                    .getAsJsonObject().get("response")
                    .getAsJsonObject().getAsJsonArray("hits").get(0)
                    .getAsJsonObject().get("result")
                    .getAsJsonObject();
            result[0] = resp.get("full_title").toString();
            result[1] = getLyrics(resp.get("url").toString());


        } catch (IOException io) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            io.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            result[0] = sStackTrace;
            result[1] = "No Lyrics Found";
        } catch (ParseException p) {
            p.printStackTrace();
            result[0] = "No Result Found - Parse error";
            result[1] = "No Lyrics Found - Parse error";
        }
        return result;
    }

    public static String getLyrics(String urls) throws IOException {
        String lyric = "";
        URL url = new URL(URLify(urls));
        Document doc = Jsoup.connect(URLify(urls)).userAgent("Mozilla").get();
        doc.outputSettings().prettyPrint(false);

        Elements elements = doc.getElementsByClass("lyrics");
        for (Element e : elements) {
            e.select("br").append("\\n");
            e.select("p").prepend("\\n\\n");
            lyric += e.text();
        }
        return lyric.replaceAll("\\\\n", "\n");
    }

    public static String URLify(String text) {
        text = text.replaceAll("^\"|\"$", "");
        if (!text.contains(" ")) {
            return text;
        }

        // Use urlifiedText for building the result text
        StringBuilder urlifiedText = new StringBuilder();

        // Replace spaces with %20, after trimming leading and trailing spaces
        for (char currentChar : text.trim().toCharArray()) {

            if (currentChar == ' ') {
                urlifiedText.append("%20");
            } else {
                urlifiedText.append(currentChar);
            }
        }

        return urlifiedText.toString();
    }


}
