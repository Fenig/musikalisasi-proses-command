package all.commands.command.Spotify;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SearchUtility extends SpotifyCommand{

    private String[] jsonResult;

    public SearchUtility(String query){
        jsonResult = getResult(query);
    }

    public String getArtistId() {
        String artistId = jsonResult[0];
        return artistId;
    }

    @Override
    protected String getApiResponse(String query) throws IOException, ParseException {
        StringBuffer response = new StringBuffer();
        String token = getToken();
        String apiResponse = null;
        String auth = "Bearer " + token;
        String searchUrl = urlByQuery(query);
        URL url = new URL(searchUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", auth);
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setUseCaches(false);
        conn.connect();
        int responseCode = conn.getResponseCode();
        if (responseCode != 200)
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        else {
            String inputLine;
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            apiResponse = response.toString();
            conn.disconnect();
        }
        return apiResponse;
    }

    @Override
    public String[] getResult(String query) {
        String[] result = new String[2];
        try {
            JSONParser parser = new JSONParser();
            JSONObject resp = (JSONObject) parser.parse(getApiResponse(query));

            JSONArray artists = (JSONArray) ((JSONObject)resp.get("artists")).get("items");
            String artistId = ((JSONObject)artists.get(0)).get("id").toString();
            System.out.println(resp.toString());
            System.out.println(artistId);
            System.out.println(resp.toString());
            JSONArray playlist = (JSONArray) ((JSONObject)resp.get("playlists")).get("items");
            String playlistId = ((JSONObject)playlist.get(0)).get("id").toString();
            result[0] = artistId;
            result[1] = playlistId;
        } catch (IOException io) {
            io.printStackTrace();
            result[0] = "IO Error";
        } catch (ParseException p) {
            p.printStackTrace();
            result[0] = "Parse Error";
        }
        return result;
    }

    public String getPlaylistId() {
        String playlistId = jsonResult[1];
        return playlistId;
    }

    public void setQuery(String query) {
        jsonResult = getResult(query);
    }

    private String urlByQuery(String query) {
        String url = search_destination_url;
        String parsedQuery = query.replaceAll(" ", "%20");
        url += "q=" + parsedQuery + "&";
        url += "type=artist%2Cplaylist&market=US&limit=5&offset=0";
        return url;
    }



}