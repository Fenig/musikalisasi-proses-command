package all.commands.command.Spotify;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public abstract class SpotifyCommand extends Command implements Spotify_OtherConstant, Spotify_URL{

    protected String getToken() throws IOException, ParseException {
        String token = null;
        URL url = new URL(token_url);
        byte[] data = createByteArrayFromString(tokenPostData);
        int postDataLength = data.length;
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("charset", "utf-8");
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setRequestProperty("Authorization", "Basic ZGM5MDZjY2I2NmY2NDI5NTg2YjVlOTYzNTliNGRlNDY6M2Y1NzQ0MzJlM2JjNDYyZmJkNmM0ZWY2MjQ5NjJlM2I=");
        conn.setUseCaches(false);
        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(data);
        }

        conn.connect();
        int responsecode = conn.getResponseCode();

        if (responsecode != 200)
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        else {
            String inputLine;

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(response.toString());
            token = jobj.get("access_token").toString();
            conn.disconnect();
        }

        return token;
    }

    protected abstract String getApiResponse(String query) throws IOException, ParseException;

    public abstract String[] getResult(String query);
}
