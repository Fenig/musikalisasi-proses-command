package all.commands.command.Spotify;

public interface Spotify_URL {
    String token_url = "https://accounts.spotify.com/api/token";
    String topArtist_destination_url = "https://api.spotify.com/v1/artists/";
    String search_destination_url = "https://api.spotify.com/v1/search?";
    String seed_base_url = "https://api.spotify.com/v1/recommendations?min_popularity=30&limit=20";
    String available_genre_url = "https://api.spotify.com/v1/recommendations/available-genre-seeds";
}
