package all.commands.command.Spotify.feature;

import all.commands.command.Spotify.SearchUtility;
import all.commands.command.Spotify.SpotifyCommand;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TopTrackCommand extends SpotifyCommand {
    /**
     * TOP TRACK masih API hard code.
     * TOP TRACK akan mengembalikan track dengan popularity tertinggi dari suatu artist yang dipilih user.
     * query pada getResult adalah nama artisnya.
     *
     * Belum diketahui cara dapet id dari artist, sehingga tidak bisa melanjutkan proses cari toptrack dari artist tersebut.
     * */
    @Override
    protected String getApiResponse(String query) throws IOException, ParseException {
        StringBuffer response = new StringBuffer();
        String token = getToken();
        String result = null;

        String auth = "Bearer " + token;

        SearchUtility sUtil = new SearchUtility(query);
        String artistId = sUtil.getArtistId();
        URL url = new URL(topArtist_destination_url + artistId + "/top-tracks?country=US");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", auth);
        conn.setRequestProperty("Accept", "application/json");
        conn.setUseCaches(false);
        conn.connect();
        int responseCode = conn.getResponseCode();


        if (responseCode != 200)
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        else {
            String inputLine;

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            result = response.toString();
            conn.disconnect();
        }
        return result;
    }

    @Override
    public String[] getResult(String query) {
        String result[] = new String[2];
        try {
            JSONParser parser = new JSONParser();
            JSONObject resp = (JSONObject) parser.parse(getApiResponse(query));
            JSONArray arr= (JSONArray)resp.get("tracks");
            resp = (JSONObject) arr.get(0);
            result[0] = resp.get("name").toString();

            String tmp = resp.get("external_urls").toString();
            resp = (JSONObject) parser.parse(tmp);
            result[1] = (String) resp.get("spotify");

        } catch (IOException io) {
            io.printStackTrace();
            result[0] = "No Result Found";
            result[1] = "No URL Found";
        } catch (ParseException p) {
            p.printStackTrace();
            result[0] = "No Result Found - Parse error";
            result[1] = "No URL Found - Parse error";
        }
        return result;
    }
}
