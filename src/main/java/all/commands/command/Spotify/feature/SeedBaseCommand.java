package all.commands.command.Spotify.feature;


import all.commands.command.Spotify.SpotifyCommand;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class SeedBaseCommand extends SpotifyCommand {
    String type = "";
    String menu = "";
    @Override
    protected String getApiResponse(String query) throws IOException, ParseException {
        menu = query;
        StringBuffer response = new StringBuffer();
        String token = getToken();
        String result = null;

        String auth = "Bearer " + token;

        String attr = "";
        if(type.equalsIgnoreCase("mood")) {
            if (menu.equalsIgnoreCase("sad")) {
                attr = "&seed_genres=sad&max_energy=0.5";
            } else if (menu.equalsIgnoreCase("happy")) {
                attr = "&seed_genres=pop&min_energy=0.7";
            } else if (menu.equalsIgnoreCase("energetic")) {
                attr = "&seed_genres=edm&min_energy=0.7&min_tempo=100";
            } else if (menu.equalsIgnoreCase("dance")) {
                attr = "&seed_genres=dance&min_energy=0.7";
            } else if (menu.equalsIgnoreCase("love")) {
                attr = "&seed_genres=pop,romance&max_energy=0.5";
            } else if (menu.equalsIgnoreCase("sleepy")) {
                attr = "&seed_genres=sleep&max_energy=0.5&max_tempo=80";
            } else if (menu.equalsIgnoreCase("angry")) {
                attr = "&seed_genres=metal&min_energy=0.7&min_tempo=100";
            } else if (menu.equalsIgnoreCase("lonely")) {
                attr = "&seed_genres=sad&max_energy=0.5";
            } else if (menu.equalsIgnoreCase("calm")) {
                attr = "&seed_genres=acoustic&max_energy=0.5&max_tempo=80";
            }
            else return "Sorry, wrong input!";
        }
        else if(type.equalsIgnoreCase("weather")) {
            if (menu.equalsIgnoreCase("sunny")) {
                attr = "&seed_genres=summer&min_energy=0.7&min_valence=0.6";
            } else if (menu.equalsIgnoreCase("rain")) {
                attr = "&seed_genres=rainy-day&max_energy=0.5&max_valence=0.5&max_tempo=80";
            } else if (menu.equalsIgnoreCase("cloudy")) {
                attr = "&seed_genres=sad,rainy_day&max_energy=0.5&max_valence=0.5";
            } else if (menu.equalsIgnoreCase("winter")) {
                attr = "&seed_genres=sad,rainy_day&max_energy=0.5&max_valence=0.5";
            }
            else return "Sorry, wrong input!";
        }
        else if(type.equalsIgnoreCase("genre")){
            if (menu.equalsIgnoreCase("edm")) {
                attr = "&seed_genres=edm";
            }
            else if (menu.equalsIgnoreCase("rap") || menu.equalsIgnoreCase("hiphop")) {
                attr = "&seed_genres=hip-hop";
            }
            else if (menu.equalsIgnoreCase("j-pop")) {
                attr = "&seed_genres=j-pop";
            }
            else if (menu.equalsIgnoreCase("jazz")) {
                attr = "&seed_genres=jazz";
            }
            else if (menu.equalsIgnoreCase("k-pop")) {
                attr = "&seed_genres=k-pop";
            }
            else if (menu.equalsIgnoreCase("international pop")) {
                attr = "&seed_genres=pop";
            }
            else if (menu.equalsIgnoreCase("rnb") || menu.equalsIgnoreCase("r n b")) {
                attr = "&seed_genres=r-n-b";
            }
            else if (menu.equalsIgnoreCase("rock")) {
                attr = "&seed_genres=rock";
            }
            else if (menu.equalsIgnoreCase("metal")) {
                attr = "&seed_genres=metal";
            }
            else return "Sorry, wrong input!";
        }


        URL url = new URL(seed_base_url+attr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", auth);
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setUseCaches(false);
        conn.connect();
        int responseCode = conn.getResponseCode();


        if (responseCode != 200)
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        else {
            String inputLine;

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            result = response.toString();
            conn.disconnect();
        }
        return result;
    }

    @Override
    public String[] getResult(String query) {
        String result[] = new String[2];
        Random randomnya = new Random();
        int randomInt = randomnya.nextInt(20);

        try {
            JSONParser parser = new JSONParser();
            String ret = getApiResponse(query);
            if(ret.equals("Sorry, wrong input!")){
                result[0] = ret;
                result[1] = "-";
                return result;
            }
            JSONObject resp = (JSONObject) parser.parse(ret);
            JSONArray arr= (JSONArray)resp.get("tracks");
            arr= (JSONArray)resp.get("tracks");
            randomInt = randomnya.nextInt(arr.size());
            resp = (JSONObject) arr.get(randomInt);
            result[0] = resp.get("name").toString();

            String tmp = resp.get("external_urls").toString();
            resp = (JSONObject) parser.parse(tmp);
            result[1] = (String) resp.get("spotify");

        } catch (IOException io) {
            io.printStackTrace();
            result[0] = "No Result Found";
            result[1] = "No URL Found";
        } catch (ParseException p) {
            p.printStackTrace();
            result[0] = "No Result Found - Parse error";
            result[1] = "No URL Found - Parse error";
        }
        return result;

    }

    public void set(String type){
        this.type = type;
    }
}
