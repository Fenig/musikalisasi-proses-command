package all.commands.command.Spotify.feature;

import all.commands.command.Spotify.SpotifyCommand;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class RandomCommand extends SpotifyCommand {
    static String[] type = {"mood", "weather", "genre"};
    static String[] mood = {"happy", "energetic", "dance", "love", "sleepy", "angry", "lonely", "calm"};
    static String[] weather = {"sunny", "rain", "cloudy", "winter"};
    static String[] genre = {"electronic", "rap", "j-pop", "jazz", "k-pop", "international pop", "rnb", "rock", "metal"};

    @Override
    protected String getApiResponse(String query) throws IOException, ParseException {
        Random rand = new Random();
        int codenumber = rand.nextInt(3);
        String result = null;
        String tempany = null;
        String temptype = type[codenumber];
        if (codenumber == 0) {
            tempany = mood[rand.nextInt(8)];
        } else if (codenumber == 1) {
            tempany = weather[rand.nextInt(4)];
        } else if (codenumber == 2) {
            tempany = genre[rand.nextInt(9)];
        }

        StringBuffer response = new StringBuffer();
        String token = getToken();

        String auth = "Bearer " + token;

        URL url = new URL(available_genre_url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", auth);
        conn.setRequestProperty("Accept", "application/json");
        conn.setUseCaches(false);
        conn.connect();
        int responseCode = conn.getResponseCode();

        if (responseCode != 200)
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        else {
            String inputLine;
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            conn.disconnect();
        }

        result = temptype + "%" + tempany;
        return result;
    }

    @Override
    public String[] getResult(String query) {
        SeedBaseCommand cari = new SeedBaseCommand();
        String[] result = new String[2];
        try {
            result = getApiResponse(query).split("%");
            cari.set(result[0]);
        } catch (IOException io) {
            io.printStackTrace();
            result[0] = "IO Error";
        } catch (ParseException p) {
            p.printStackTrace();
            result[0] = "Parse Error";
        }
        return cari.getResult(result[1]);
    }
}
