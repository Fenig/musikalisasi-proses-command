package all.commands.command.Spotify;

public class ReturnObject {
    private String result;
    private String url;

    public ReturnObject() {}

    public ReturnObject(String result, String url) {
        this.result = result;
        this.url = url;
    }

    public String getResult() {
        return this.result;
    }

    public String getUrl() {
        return this.url;
    }

}