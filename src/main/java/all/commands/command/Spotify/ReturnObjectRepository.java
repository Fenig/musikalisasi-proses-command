package all.commands.command.Spotify;

import org.springframework.data.jpa.repository.JpaRepository;

interface ReturnObjectRepository extends JpaRepository<ReturnObject, Long> {
}
