package all;

import all.commands.command.Genius.GeniusCommand;
import all.commands.command.Spotify.ReturnObject;
import all.commands.command.Spotify.feature.RandomCommand;
import all.commands.command.Spotify.feature.SeedBaseCommand;
import all.commands.command.Spotify.feature.TopTrackCommand;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
public class ServiceController {

    @GetMapping("/toptrack/{query}/")
    public ReturnObject topTrackController(@PathVariable("query") String query) {
        TopTrackCommand command = new TopTrackCommand();
        String result[] = command.getResult(query);
        ReturnObject ret = new ReturnObject(result[0], result[1]);
        return ret;
    }

    @RequestMapping(value = "/lyrics/{query}", method=RequestMethod.GET)
    public ReturnObject lyricController(@PathVariable("query") String query) {
        GeniusCommand command = new GeniusCommand();
        String result[] = command.getResult(query);
        ReturnObject ret = new ReturnObject(result[0], result[1]);
        return ret;
    }

    @RequestMapping(value = "/mood/{query}", method=RequestMethod.GET)
    public ReturnObject seedMoodController(@PathVariable("query") String query) {
        SeedBaseCommand command = new SeedBaseCommand();
        command.set("mood");
        String result[] = command.getResult(query);
        ReturnObject ret = new ReturnObject(result[0], result[1]);
        return ret;
    }
    @RequestMapping(value = "/genre/{query}", method=RequestMethod.GET)
    public ReturnObject seedGenreController(@PathVariable("query") String query) {
        SeedBaseCommand command = new SeedBaseCommand();
        command.set("genre");
        String result[] = command.getResult(query);
        ReturnObject ret = new ReturnObject(result[0], result[1]);
        return ret;
    }
    @RequestMapping(value = "/weather/{query}", method=RequestMethod.GET)
    public ReturnObject seedWeatherController(@PathVariable("query") String query) {
        SeedBaseCommand command = new SeedBaseCommand();
        command.set("weather");
        String result[] = command.getResult(query);
        ReturnObject ret = new ReturnObject(result[0], result[1]);
        return ret;
    }

    @RequestMapping(value = "/random/{query}", method=RequestMethod.GET)
    public ReturnObject randomCommandController(@PathVariable("query") String query) {
        RandomCommand command = new RandomCommand();
        String result[] = command.getResult(query);
        ReturnObject ret = new ReturnObject(result[0], result[1]);
        return ret;
    }
}
